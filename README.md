# README #
Google Analytics Tutorial for IOS without configuration file

## What do I need? ##

1. Google tracker Number (such as UA-xxxxxxxx-x)
2. CocoPod setup with 
```
#!objective-c

pod 'Google/Analytics'

```

You may follow the library setup here, skip the configuration file.
[Google Analytic for IOS](https://developers.google.com/analytics/devguides/collection/ios/v3/)

```
#!objective-c

pod init
Open the Podfile created for your application and add the following:

pod 'Google/Analytics'
Save the file and run:

pod install
This creates an .xcworkspace file for your application. Use this file for all future development on your application.
```

## How do I Setup? ##

### 1. Firstly setup your project with required library ###

### 2. Setup your Application Delegate ###


In your Delegate class you will need to define your tracker ID and initialise it. Create a session is mandatory.
Note that you will need to make your tracker accessible somewhere, or use it as a singleton if you need to.

Tracker property is defined as :

```
#!objective-c

@property (strong, nonatomic) id<GAITracker> mGAITracker;

```

Delegate Example:


```
#!objective-c

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    //Get GA instance
    GAI *gai = [GAI sharedInstance];

     //set your dispatch time for your google analytic, this will be the realtime responsiveness value
     //Here we set to 5 seconds to reduce number of out going requests.
    [gai setDispatchInterval:5];
    
    _mGAITracker = [gai trackerWithTrackingId:@"UA-73077333-1"];//set tracker number
    //gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // for logging

    //Create a session, mandatory
    [_mGAITracker send:[[GAIDictionaryBuilder createScreenView] build]];
    return YES;
}

```

### 3. Adding events ###

This will appear on your Event section of google analytic



```
#!objective-c

[_app.mGAITracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"
                                                                               action:@"Eat Icecream"
                                                                                label:sender.titleLabel.text
                                                                                value:nil] build]];

```

### 6. Adding Screen name ###

This will appear on your Screen section of google analytic


```
#!objective-c

[tracker set:kGAIScreenName value:@"my photo viewing "];
[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
```