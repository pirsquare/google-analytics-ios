//
//  Copyright (c) 2015 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <Google/Analytics.h>

@interface ViewController ()
@property AppDelegate * app;
@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    
    self.app = [UIApplication sharedApplication].delegate;

}

- (IBAction)analyticButtonHit:(UIButton*)sender {
    
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
    NSString *dateString = [dateFormat stringFromDate:today];
    
    [_app.mGAITracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"
                                                                               action:dateString
                                                                                label:sender.titleLabel.text
                                                                                value:nil] build]];
}
@end
