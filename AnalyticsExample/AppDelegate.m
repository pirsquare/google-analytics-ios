 
#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    GAI *gai = [GAI sharedInstance];
    [gai setDispatchInterval:5];//will send all after x Second
    
    _mGAITracker = [gai trackerWithTrackingId:@"UA-73077333-1"];//set tracker number
    //gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // for logging
    [_mGAITracker send:[[GAIDictionaryBuilder createScreenView] build]];
    return YES;
}

@end
